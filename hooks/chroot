#!/bin/sh
set -e

# Hook for building live images
#
# This script is run in the built chroot after all packages are installed,
# before it is packed into a squashfs. This is where you can apply extra tweaks
# to the live system.
echo 'export USERFULLNAME="Baltix Live user"' >> /etc/casper.conf
echo 'export HOST="baltix"' >> /etc/casper.conf
#echo "deb http://download.videolan.org/pub/debian/stable/ /" >> /etc/apt/sources.list.d/videolan-dvdcss.list && wget -O - http://download.videolan.org/pub/debian/videolan-apt.asc|apt-key add -

#Warning: apt-key output should not be parsed (stdout is not a terminal)
#gpg: keyserver receive failed: End of file
# onlyoffice-desktopeditors repository key:
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CB2DE8E5 || true
echo "#deb https://download.onlyoffice.com/repo/debian squeeze main" > /etc/apt/sources.list.d/onlyoffice.list

echo 'GRUB_RECORDFAIL_TIMEOUT=5' >> /etc/default/grub

# Latest Mozilla Firefox, Firefox ESR and Thunderbird deb releases:
add-apt-repository --yes --no-update ppa:mozillateam/ppa

#apt-get --quiet update
#apt-get  --yes install libdvdcss2
echo msttcorefonts msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
#Ubuntu 20.04 bugova versija pakimba siuntimas kai bando patį pirmą andale siųsti,rašo waiting for headers
#apt-get -q --yes install ttf-mscorefonts-installer || true
wget -nv http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.8_all.deb -O /tmp/ttf-mscorefonts-installer_3.8_all.deb
dpkg -i /tmp/ttf-mscorefonts-installer_3.8_all.deb || true
apt-get -q --yes -f install
rm -vf /tmp/ttf-mscorefonts-installer_3.8_all.deb

# fonts-tlwg-waree-ttf+ contains Lithuanian symbols
apt-get -q purge --auto-remove -y fonts-noto-cjk fonts-nanum fonts-khmeros-core fonts-thai-tlwg fonts-tlwg-\* fonts-tlwg-waree-ttf+ fonts-nakula fonts-sil-abyssinica fonts-sil-padauk fonts-kacst fonts-tibetan-machine fonts-gujr fonts-gujr-extra fonts-beng fonts-beng-extra fonts-smc-\* fonts-deva-extra fonts-gargi fonts-sarai fonts-gubbi fonts-lklug-sinhala fonts-telu fonts-telu-extra fonts-guru fonts-pagul fonts-samyak-gujr fonts-samyak-mlym fonts-samyak-taml fonts-lohit-mlym
# language-pack-zh-hans language-pack-gnome-zh-hans language-pack-es language-pack-gnome-es language-pack-pt language-pack-gnome-pt firefox-locale-zh-hans firefox-locale-es firefox-locale-pt
# Maybe ibus-xkbc could be installed for correct GNOME Shell behaviour
# ibus-xkbc is included into new ibus, see https://bugs.debian.org/751017
apt-get -q purge --auto-remove -y libreoffice-help-zh-tw libreoffice-l10n-en-za ibus-pinyin libpinyin-data mozc-server ibus-data libhangul-data gtk-im-libthai
# Removing unneeded oss-compat and osspd, which are recommended by old libdv-bin and huge fluid-soundfont-gm
apt-get -q purge --auto-remove -y oss-compat osspd fluid-soundfont-gm minuet minuet-data freepats timidity # libtelepathy-glib0 || true
# Nežinau ar mest libtelepathy-glib0, nuo jo priklauso gir1.2-folks-0.6, kuris tik rekomenduojamas gnome-shell-extension-gsconnect

#Removing unneeded recommended packages: gawk may needed for boot-repair, see LP #1331381
# mysql-common can't be removed in Ubuntu 20.04, see bug 
apt-get -q purge --auto-remove -y dpkg-dev g++ libc6-dbg gdb linux-libc-dev manpages-dev gnome-package-updater mysql-common || true
# Removing unneeded packages since 12.04.1
#apt-get purge --auto-remove -y libmusicbrainz3-6 liborbit2 libavahi-ui-gtk3-0 libidl0 libidl-common
# Removing unneeded packages since 16.04 (because ubuntu-desktop recommends libqt4-sql-sqlite
apt-get -q purge --auto-remove -y libqtcore4 ubuntu-touch-sounds ubuntu-mobile-icons postfix || true
# Hardinfo package recommends lm-sensors, but lm-sensors sets sensors limits at startup
# I'm not sure if sensors -s at computer startup won't cause problems on some systems
# qt-at-spi causes crash in QT file open/save dialogs, see http://launchpad.net/bugs/998012
# gdm isn't needed because lightdm is as default, liboxideqtcore0 is only for unity webapps
apt-get -q purge --auto-remove -y lm-sensors apport unity-webapps-common || true

#Error if purging whoopsie: rm: cannot remove '/var/lib/whoopsie/whoopsie-id': No such file
apt-get -q remove --auto-remove -y whoopsie || true
#apt-get --no-install-recommends install mdadm
# downgrading linux-firmware-nonfree to save some space in 12.04 LiveCD
#apt-get --force-yes --yes install linux-firmware-nonfree=1.11
apt-get --quiet update || true
# Ubuntu-defaults-baltix package contains custom /etc/locale.nopurge config
# to remove unneeded non-european translations
test -x /usr/share/ubuntu-defaults-baltix/hooks/localepurge && /usr/share/ubuntu-defaults-baltix/hooks/localepurge
#Lowers memory usage in Live session by removing unneeded shell prompts on virtual consoles
#test -w /usr/share/initramfs-tools/scripts/casper-bottom/25configure_init && sed -i -e "s|^log_end_msg.*|rm -f /root/etc/init/tty3.conf /root/etc/init/tty4.conf /root/etc/init/tty5.conf /root/etc/init/tty6.conf\n\n&|" /usr/share/initramfs-tools/scripts/casper-bottom/25configure_init

# Show more examples for user
#ln -s /usr/share/checkbox/data/documents /usr/share/example-content/Documents
test -d /usr/share/example-content && ln -s /usr/share/backgrounds /usr/share/example-content

# Translate LibreOffice launcher to Lithuanian language - 
# Lithuanian users should find office suite by typing rašyk ir skaičiuok
#sed --follow-symlinks -i '/^Name\[lt\]/d' /usr/share/applications/libreoffice-startcenter.desktop
sed --follow-symlinks -i 's/^Comment\[lt\]=.*$/Comment\[lt\]=Biuro programų rinkinys (rašyklė, skaičiuoklė, pateikčių rengyklė, braižymo, duomenų bazių ir kt.), palaikantis OpenDocument ir kitus dokumentų formatus/' /usr/share/applications/libreoffice-startcenter.desktop
#echo "Name[lt]=Raštinės programos „LibreOffice“">>/usr/share/applications/libreoffice-startcenter.desktop

# Software-properties (repositories+updates and additional drivers settings) 
# should be visible also in GNOME-Control-Center - workaroud for LP bug #1336326
#sed --follow-symlinks -i 's/^Categories=GNOME;GTK;Settings;X-GNOME-SystemSettings;$/Categories=GNOME;GTK;Settings;X-GNOME-SystemSettings;X-GNOME-Settings-Panel;/' /usr/share/applications/software-properties-gnome.desktop
#sed --follow-symlinks -i '/^X-GNOME-Settings-Panel=.*$/d' /usr/share/applications/software-properties-gnome.desktop
#echo "X-GNOME-Settings-Panel=software-properties" >> /usr/share/applications/software-properties-gnome.desktop

# Remove unneeded translations of LibreOffice Letter wizard
# TODO: bugreport: these files should go to libreoffice language packs
for lang in bg eu hu ja km ko pt pt-BR tr vi zh-CN zh-TW; do
 rm -rfv /usr/lib/libreoffice/share/template/wizard/letter/$lang
done

# Remove not needed big files from brltty tool
rm -fv /etc/brltty/zh-tw* /etc/brltty/ko.ctb /usr/share/doc/brltty/French/* /usr/share/doc/brltty/English/BRLTTY.txt.gz

# Remove not needed notification from nautilus-admin
#rm -fv /var/lib/update-notifier/user.d/nautilus-admin-restart

# Copy updated (not identical) firmware files from /lib/firmware/3.8.0-*/ to 
# /lib/firmware in hooks/chroot (duplicated files doesn't use space in compressed filesystem)
#for firmware in phanfw.bin ql2500_fw.bin ql2400_fw.bin ql2322_fw.bin ql2300_fw.bin "mrvl/sd8787_uapsta.bin"; do
# cmp /lib/firmware/3.8.0-*/$firmware /lib/firmware/$firmware || cp /lib/firmware/3.8.0-*/$firmware /lib/firmware/$firmware
#done

# checkbox/data/video/* wastes 800Kb and aren't needed for user
CHECKBOXFOLDER="/usr/share/checkbox/data/video"
test -d $CHECKBOXFOLDER || CHECKBOXFOLDER="/usr/share/2013.com.canonical.certification:checkbox/data/video"
rm -fv $CHECKBOXFOLDER/Ogg_Theora_Video.ogv && test -d $CHECKBOXFOLDER && ln -s "/usr/share/example-content/Ubuntu_Free_Culture_Showcase/How fast.ogg" "$CHECKBOXFOLDER/Ogg_Theora_Video.ogv"

# Remove big changelog.gz and other unneeded files from doc folder
rm -fv /usr/share/doc/rhythmbox-mozilla/changelog.gz /usr/share/doc/rhythmbox-plugin-cdrecorder/changelog.gz /usr/share/doc/librhythmbox-core6/changelog.gz /usr/share/doc/gir1.2-rb-3.0/changelog.gz /usr/share/doc/rhythmbox-data/changelog.gz /usr/share/doc/rhythmbox-plugin-zeitgeist/changelog.gz /usr/share/doc/rhythmbox-plugin-magnatune/changelog.gz /usr/share/doc/rhythmbox/changelog.gz /usr/share/doc/rhythmbox-plugins/changelog.gz
rm -fv /usr/share/doc-base/xterm-faq /usr/share/doc/xterm/xterm.faq.html /usr/share/doc/xterm/xterm.log.html /usr/share/doc/xterm/xterm.faq.gz
rm -fv /usr/share/doc/libgtk2.0-0/changelog.gz /usr/share/doc/gcc-4.6-base/changelog.gz /usr/share/doc/libgd2-xpm/README.html
rm -fv /usr/share/doc-base/configobj-api /usr/share/doc/python-configobj/api/* /usr/share/doc/python-configobj/api/configobj-pysrc.html /usr/share/doc/python-configobj/configobj.html /usr/share/doc/python-configobj/validate.html
rm -fv /usr/share/doc/libsane/html/sane-backends.html /usr/share/doc/libsane/umax/sane-umax-*a*.jpg /usr/share/doc/libsane/umax/sane-umax.jpg
rm -fv /usr/share/doc/printer-driver-foo2zjs/manual.pdf /usr/share/doc-base/foo2zjs
rm -fv /usr/share/doc/syslinux/logo/syslinux-100.png /usr/share/doc/syslinux/CodingStyle.txt.gz
rm -fv /usr/share/doc/shared-mime-info/shared-mime-info-spec.pdf /usr/share/doc-base/shared-mime-info
for lang in bg ca cs eu el es it hu ja km ko pt pt-BR tr vi zh-CN zh-TW sv; do
 rm -rfv /usr/share/gnome/help/drivemount/$lang
 rm -rfv /usr/share/gnome/help/invest-applet/$lang
 rm -rfv /usr/share/gnome/help/trashapplet/$lang
 rm -rfv /usr/share/gnome/help/gstreamer-properties/$lang
 rm -rfv /usr/share/gnome/help/rhythmbox/$lang
 rm -rfv /usr/share/gnome/help/gweather/$lang
 rm -rfv /usr/share/gnome/help/stickynotes_applet/$lang
 rm -rfv /usr/share/gnome/help/multiload/$lang
 rm -rfv /usr/share/gnome/help/char-palette/$lang
 rm -rfv /usr/share/gnome/help/gnome-search-tool/$lang
 rm -rfv /usr/share/gnome/help/cpufreq-applet/$lang
done

apt-get purge --auto-remove -y

dpkg --clear-avail
dpkg --clear-avail
