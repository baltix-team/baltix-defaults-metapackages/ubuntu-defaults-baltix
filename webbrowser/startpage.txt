# The following URL will become the web browser start page.
# For now only Firefox is supported.
#
# Format: single line with an URL. Using the ${distro_release_number}
# or ${distro_release_name} macros is often useful here.
#
# Example:
# http://www.ubuntuforums.org?release=${distro_release_number}
#
# Should set Firefox Home Page to default, because it's translated to all Euro
# languages, looks better and is more functional than Ubuntu's about:startpage
# But setting here to about:home couses problems, so, I simply copied 
# /usr/share/xul-ext/ubufox/defaults/preferences/* from ubuntu-gnome-default-settings 13.10
# See bug LP: #1195367 (Firefox about:home doesn't work right when set with ubuntu-defaults-builder)